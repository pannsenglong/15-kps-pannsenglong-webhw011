import React, { Component } from 'react'
import {ListGroup} from 'react-bootstrap'

export default class HistoryResult extends Component {
    render() {
        return (
            <div>
                <ListGroup.Item style={{margin: '10px 0px'}}> {this.props.data}</ListGroup.Item>
            </div>
        )
    }
}
