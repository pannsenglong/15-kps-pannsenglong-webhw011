import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card,Button,Form, Container, Row, Col} from 'react-bootstrap'
import '../style.css'
import HistoryResult from './HistoryResult'
import {ListGroup} from 'react-bootstrap'

export default class Calculator extends Component {
    constructor(){
        super()
        this.state = {
            number1: 0,
            number2: 0,
            result: 0,
            operator: '+ Plus',
            history: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.calculate = this.calculate.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    validate = () => {
        let partern = /^[0-9]*$/
        let isOk = true;
        let num1 = this.state.number1
        let num2 = this.state.number2
        
        if(num1 === ''){
            alert('please input first number.')
            isOk = false;
        }
        else if(num2 === ''){
            alert('please input second number.')
            isOk = false
        }
        else if(!num1.match(partern))
        {
            alert('please input valid number')
            isOk = false;
        }
        else if(!num2.match(partern))
        {
            alert('please input valid number')
            isOk = false
        }
        return isOk;
    }

    calculate = () =>
    {
        let result = 0
        let operation;
        if(this.validate())
        {
            switch(this.state.operator)
            {
                case '+ Plus':  
                    result = Number(this.state.number1) + Number(this.state.number2)
                    operation = ' + ';
                    break;  
                case '- Subtract':  
                    result = this.state.number1 - this.state.number2 
                    operation = ' - ';
                    break;  
                case '* Multiply':  
                    result = this.state.number1 * this.state.number2 
                    operation = ' x ';
                    break;  
                case '/ Divide':  
                    result = this.state.number1 / this.state.number2  
                    operation = ' ៖ ';
                    break;  
                case '% Module':
                    result = this.state.number1 % this.state.number2
                    operation = ' % ';
                    break;
                default:
                    console.log("DEFAULT");    
                    break;
            }
            let resultString = this.state.number1 + " " + operation + " " + this.state.number2 + ' = ' + result;
            this.setState({history: this.state.history.concat(resultString)})
        }         
    }

    render() {
        let item = this.state.history.map((numbers, index) => (
            <HistoryResult data={numbers} key={index} />
          ));
      
        return (
            <Container style={{paddingTop: '30px'}}>
                <Row>
                    <Col md='6' sm= '12' xl= '6'>  
                        <Card style={{ width: '23rem' }}>
                             <Card.Img variant="top" src="./Google_stock_calculator_app_icon.png" />
                                <Card.Body>
                                    <Form.Control style={{margin: '20px 0px'}} type="email" name='number1' value={this.state.value} placeholder="Input Number" onChange={this.handleChange}/>
                                    <Form.Control style={{margin: '20px 0px'}} type="email" name='number2' value={this.state.value} placeholder="Input Number" onChange={this.handleChange}/>
                                    <Form.Control tyle={{margin: '20px 0px'}} as="select" name="operator" value={this.state.value} onChange={this.handleChange}>
                                        <option>+ Plus</option>
                                        <option>- Subtract</option>
                                        <option>* Multiply </option>
                                        <option>/ Divide</option>
                                        <option>% Module</option>
                                    </Form.Control>   
                                <Button style={{margin: '20px 0px'}} variant="primary" onClick={this.calculate}>Calculate</Button>
                            </Card.Body>
                        </Card>  
                    </Col>
                    <Col md='6' sm= '12' xl= '6'>  
                        <h3 style={{textAlign: "center", paddingTop: 20}}>Result History</h3>
                        <ListGroup>{item}</ListGroup>
                    </Col>
                </Row>
            </Container>
        )
    }
}
